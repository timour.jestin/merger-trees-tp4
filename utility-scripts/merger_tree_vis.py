import numpy as np
import tangos

# default_setup should be changed to your preferred matplotlib style file
#import default_setup
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors
from matplotlib.pyplot import cm
from matplotlib.colors import LogNorm
plt.ion()

import pydot

# Ensure that Pydot is installed to python3
# Ensure that GraphViz is loaded

# Set plot parameters:
#--------------------------------------------------------------------
# Retrieve the desired tangos entry (Tangos database must have M200c): 

sim = '<sim_name>' 

print("\n#--------------------------------------------------------------------\nsimulation ",sim,"\n#--------------------------------------------------------------------\n")

max_snapshot = len(tangos.get_simulation(sim).timesteps)-1


num_max = 0
redshift_threshold = 0.0
# adds a threshold after which the tree is not plotted (e.g for 1, the tree will be ploted on [z_max, 1])

for i in tangos.get_simulation(sim).timesteps: 
  if i.redshift >= redshift_threshold:
    num_max += 1
max_snapshot = num_max - 1


#print(max_snapshot)
min_snapshot = 0
main_halo = 0
cprop = 'Spin' # Colourbar property (will be logged) ------------ 'SFR_100Myr'
cprop_max = None
cprop_min = None

min_merger_ratio = 1/100. # Minimum merger ratio (evaluated using NDM)
min_mass_ratio = 1e-3 # Minimum tree mass as a percentage of the root mass

# Plotting options:

scale_dots = 1.2 #default 1, dot diameter multiplier
scaleup = 5 #default 1, vertical scale multiplier



fs = 10
aspect = None # Enforce an aspect ratio for the plot
label_halos = False # Label each node with the halo and snapshot number
line_colour = 'lightgrey'
main_prog_line_colour = 'red'
direction = 'right to left' # "left to right" or "right to left"

# Pydot keyword arguments, do not change these:
dpi = 10
dot_kwargs = {'rankdir': 'LR', 'nodesep': 1, 'ranksep': '1', 'splines': 'spline', 'outputorder': 'edgesfirst', 'dpi': dpi, 'pad': 1}
node_kwargs = {"label":'', "shape": "square", "width": 0.01, "height": 0.01}
edge_kwargs = {"penwidth": 0}

# Design green-purple-blue cmap:
cdict = {'red': ((0.0, 0.0, 0.0),
                 (0.1, 0.75, 0.75),
                 (1.0, 0.0, 0.0)),
        'green':((0.0, 0.0, 0.0),
                 (0.1, 0.0, 0.0),
                 (1.0, 1.0, 1.0)),
        'blue': ((0.0, 1.0, 1.0),
                 (0.1, 1.0, 1.0),
                 (1.0, 0.47, 0.47))}
cmap = mpl.colors.LinearSegmentedColormap('my_colormap', cdict, 256)
#--------------------------------------------------------------------

# Load tangos halo:
#--------------------------------------------------------------------
# print(sim + '/snapshot_%03d' % max_snapshot + '/halo_%i' % main_halo)

# h = tangos.get_halo(sim + '/snapshot_%03d' % max_snapshot + '/halo_%i' % main_halo)

# print(max_snapshot - min_snapshot)

#print(tangos.get_simulation(sim).timesteps[max_snapshot - min_snapshot])

h = tangos.get_simulation(sim).timesteps[max_snapshot - min_snapshot].halos[main_halo]

# Create mappings between snapshots and times (will be useful later):
step_ids = np.array([i.id for i in tangos.get_simulation(sim).timesteps])

snap_num = np.array([int(str(i).split('snapshot_')[1][:3]) for i in tangos.get_simulation(sim).timesteps])
#print(snap_num.shape)
outputs = {}
tree_redshifts = {}
for i in range(len(snap_num)):
  #print(step_ids[i])
  outputs[step_ids[i]] = snap_num[i]
#print(outputs)
for i in tangos.get_simulation(sim).timesteps:
  #print(int(i.extension[-2:]))
  tree_redshifts[int(i.extension[-3:])] = abs(i.redshift)
  #print(abs(i.redshift))

#print(len(tree_redshifts))

""" 
import sys
sys.exit()
"""
#--------------------------------------------------------------------

# Time conversion calculations:
#--------------------------------------------------------------------
import scipy.integrate
from scipy.optimize import newton
conv = 9.77813951206781 # Solar mass per Gyr units

def _a_dot(a, h0, om_m, om_l):
  om_k = 1.0 - om_m - om_l
  return h0 * a * np.sqrt(om_m * (a ** -3) + om_k * (a ** -2) + om_l)

def _a_dot_recip(*args):
  return 1. / _a_dot(*args)

def func(a, time):
  return age_time(a) - time

def time_age(time):
  return np.array([newton(func, 1, args=([x])) for x in time])

def age_time(a, omegaM=0.315000, omegaL=0.685000, h=0.673000):
  return scipy.integrate.quad(_a_dot_recip, 0, a, (h, omegaM, omegaL))[0] * conv
#--------------------------------------------------------------------

# Misc Functions:
#--------------------------------------------------------------------
# Sigmoid function for smooth pydot edges:
def sigmoid(x, beta=3.):
  return 1 / (1 + (x / (1-x))**(-beta))
x = sigmoid(np.linspace(0 + 1e-5, 1 - 1e-5, 20))

# Convenient normalisation function:
def normalise(a, normmin, normmax, vmin, vmax):
  normed = (a-vmin) / (vmax-vmin) * (normmax-normmin) + normmin
  normed[normed > normmax] = normmax
  normed[normed < normmin] = normmin
  return normed

# Translate numbers into hex colours:
def get_colour(cprops, cprop_min, cprop_max):
  cprop_normed = normalise(cprops+1e-10, 0, 99, cprop_min, cprop_max) #normalise(np.log10(cprops+1e-10), 0, 99, cprop_min, cprop_max)
  color = colours[np.round(cprop_normed).astype('int')]
  return np.array(list(map(matplotlib.colors.rgb2hex, color)))

# Reformat scientific notation:
def latex_float(f):
    float_str = "{0:.1e}".format(f)
    if "e" in float_str:
        base, exponent = float_str.split("e")
        return r"{0} \times 10^{{{1}}}".format(base, int(exponent))
    else:
        return float_str

# Get the tree indexes for the main progenitor line:
def main_prog_line(progline, progmass, progstep, halo):
  try:
    progmass.append(halo['M200c'])
    progline.append(halo.id)
    progstep.append(halo.timestep_id)
  except:
    return
  halo = halo.previous
  if halo is not None:
    main_prog_line(progline, progmass, progstep, halo=halo)
  else:
    return

# Add pydot node:
def plot_node(halo, graph):
  node_name = f"{halo}"
  my_node = graph.get_node(node_name)
  if len(my_node) == 0:
    my_node = pydot.Node(node_name, **node_kwargs)
    graph.add_node(my_node)
  else:
    my_node = my_node[0]
  return my_node

# Walk through tree and find the necessary properties:
def get_ancestors(halo, i, graph, my_node, desc_ID):


  # Add new node:
  my_node = plot_node(halo.id, graph)
  desc_ID = halo.id

  # Find all of the ancestors to the current halo:
  links = halo.all_links
  ancestors = np.array([j.halo_to for j in links])
  progenitor = np.array([j.next == halo for j in ancestors])
  same_sim = np.array([sim in j.path for j in ancestors])
  mass_ratio = np.array([(j.NDM / halo.NDM > min_merger_ratio) & (j.NDM / root_NDM > min_mass_ratio) for j in ancestors])
  #print(progenitor)
  #print(same_sim)
  #print(mass_ratio)
  ancestors = ancestors[progenitor & same_sim & mass_ratio]

  # End if there are no ancestors at all:
  if not len(ancestors):
    return

  # Loop over each ancestor:
  for anc in ancestors:

    # End if there is no mass property in the database:
    try: anc['M200c']
    except: return

    anc_node = plot_node(anc.id, graph)
    mass.append(anc['M200c'])
    SnapNum.append(outputs[anc.timestep_id])
    ID.append(anc.id)
    new_snap.append(i-1)
    HaloNum.append(int(anc.basename.split('_')[1]))

    try: cprops.append(anc[cprop])
    except: cprops.append(0.)

    if anc.next is not None:
      desc.append(desc_ID)
    else:
      desc.append(0)
      return

    # Add connecting pydot edges:
    graph.add_edge(pydot.Edge(my_node, anc_node, **edge_kwargs))

    # Iterate recursively:
    get_ancestors(anc, i-1, graph, my_node, desc_ID)

#--------------------------------------------------------------------

# Find tree properties:
#--------------------------------------------------------------------
print('>    Finding tree properties.')

# Initialise data storage lists with the properties of the first halo:
print(h)
root_NDM = h.NDM
mass = [h['M200c']]


SnapNum = [outputs[h.timestep_id]]
desc = [0]
ID = [h.id]
cprops = [h[cprop]]
new_snap = [max_snapshot]
HaloNum = [int(h.basename.split('_')[1])]

# Find the necessary main progenitor properties in advance:
progline = []
progmass = []
progstep = []
main_prog_line(progline, progmass, progstep, h)

# Set up a pydot graph object:
graph = pydot.Dot(graph_type='graph', **dot_kwargs)

# Walk through the tree:
get_ancestors(h, max_snapshot, graph, None, None)

# Convert lists to numpy arrays:
mass = np.array(mass)
SnapNum = np.array(SnapNum)
new_snap = np.array(new_snap)
desc = np.array(desc)
ID = np.array(ID)
cprops = np.array(cprops)
HaloNum = np.array(HaloNum)

# Render:
filename = sim + '_tree.txt'
func = getattr(graph, f"write_plain", None)
func(filename)

# Interpret the node positions from the pydot .txt file:
with open(filename, 'r') as file:
  lines = file.readlines()
lines = np.array(lines)[['node' in i for i in lines]]
node_IDs = [int(line.split(' ')[1]) for line in lines]
order = [float(line.split(' ')[3]) for line in lines]
order = np.array(order)[[np.where(node_ID == ID)[0][0] for node_ID in node_IDs]]

#print(order)
print("scaleup = ",scaleup)
order = order * scaleup
# Make the plot:
#--------------------------------------------------------------------
print('>    Building plot.')

fig_size = (len(np.unique(SnapNum))-1) / 7.
if aspect is not None:
  axis_ratio = aspect
else:
  axis_ratio = (SnapNum.max() - SnapNum.min()) / (order.max() - order.min())
fig, ax = plt.subplots(figsize=(fig_size, fig_size/axis_ratio))

# Set up the size properties:



min_dot_size = 0.5 * scale_dots
max_dot_size = 7 * scale_dots
min_edge_size = 0.002 * scale_dots
max_edge_size = (1/3) * scale_dots
max_mass = np.log10(mass.max())
min_mass = np.log10(mass.max() * min_mass_ratio)
min_mass = np.log10(mass.min())
dot_size = normalise(np.log10(mass), min_dot_size, max_dot_size, min_mass, max_mass)

'''
print(mass.shape)
print(mass)
print(min_dot_size)
print(max_dot_size)
print(min_mass)
print(max_mass)
'''

edge_size = normalise(dot_size**2, min_edge_size, max_edge_size, dot_size.min()**2, dot_size.max()**2)

# Set up the colourbar properties:
colours = cmap(np.linspace(0, 1, 100))
if cprop_max is None:
  cprop_max = cprops.max() #np.log10(cprops).max()
if cprop_min is None:
  cprop_min = cprops.min() #np.log10(cprops).min()
c = get_colour(cprops, cprop_min, cprop_max)

# Loop over each snapshot and plot the nodes/connections in matplotlib:
i = 0
rank = SnapNum == SnapNum.max()

'''
print(dot_size[rank].shape)
print(dot_size.shape)
print(order[rank].shape)
print(order.shape)
print(rank.shape)
'''

ax.scatter(SnapNum[rank], order[rank], s=dot_size[rank]**2, c=c[rank], lw=0, clip_on=False)
for snap in range(SnapNum.max()-1, SnapNum.min()-1, -1):

  # Find the current timestep and the descendants:
  rank = SnapNum == snap
  next_rank = [np.where(ID == j)[0][0] for j in desc[rank]]

  # Draw the nodes:
  ax.scatter(SnapNum[rank], order[rank], s=dot_size[rank]**2, c=c[rank], lw=0, clip_on=False)

  # Draw the connecting lines:
  for j in range(np.sum(rank)):
    if label_halos:
      string = '%i' % HaloNum[rank][j] + '\n' + '%i' % SnapNum[rank][j]
      ax.text(SnapNum[rank][j], order[rank][j], string, fontsize=1*scale_dots, ha='center', va='center')

    # Use a different line colour for the main progenitor line:
    edge_c = line_colour
    zorder=-1
    if i+1 < len(progline):
      if ID[rank][j] == progline[i+1]:
        edge_c = main_prog_line_colour
        zorder=0
        i += 1

    # Filled sigmoid edges for stylistic purposes, but a sigmoid linecollection with variable width works too:
    y = np.linspace(SnapNum[rank][j], SnapNum[next_rank][j], 20)
    x_rank, x_next_rank = order[rank][j], order[next_rank][j]
    x_up = (x_rank+edge_size[rank][j]) + (x * ((x_next_rank+edge_size[next_rank][j]) - (x_rank+edge_size[rank][j])))
    x_down = (x_rank-edge_size[rank][j]) + (x * ((x_next_rank-edge_size[next_rank][j]) - (x_rank-edge_size[rank][j])))
    ax.fill_between(y, x_up, x_down, color=edge_c, zorder=zorder, clip_on=False)
#--------------------------------------------------------------------

# Time axis:
#--------------------------------------------------------------------
redshift_ticks = None # Set to a list of redshift ticks
n_ticks = 10 # Number of ticks
tick_precision = 3 # Number of significant figures for the ticks
timebar_padding = 0. # Padding between merger tree and axis, vertical fraction

# Define tick locations:
snapshots = np.unique(SnapNum)
redshifts = np.abs([tree_redshifts[i] for i in snapshots])
if redshift_ticks is not None:
  new_tick_locations = np.interp(redshift_ticks, redshifts[::-1], snapshots[::-1])
else:
  new_tick_locations = np.linspace(*snapshots[[0,-1]], n_ticks)
new_tick_labels = np.interp(new_tick_locations, snapshots, redshifts)

# Create ticks:
ax.set_xticks(new_tick_locations)
ax.set_xticklabels(['%.2g' % round(i, tick_precision) for i in new_tick_labels])
ax.set_xlabel(r'Redshift', fontsize=fs)
ax.tick_params(axis='x', labelsize=fs-2)

# Remove unecessary spines and ticks:
for spine in ['left', 'right', 'top']:
  ax.spines[spine].set_visible(False)
ax.set_yticks([])
ax.tick_params(axis='x', which='both', direction='out')
ax.tick_params(top=False, which='both')
ax.tick_params(bottom=False, which='minor')
ax.minorticks_off()

# Control the direction:
if direction == 'left to right':
  ax.set_xlim(new_tick_locations[[-1,0]])
elif direction == 'right to left':
  ax.set_xlim(new_tick_locations[[0,-1]])

# Twinned axis for linear time:
times = [age_time(1/(1+i)) for i in new_tick_labels]
time_ax = ax.twiny()
time_ax.set_position(ax.get_position())
time_ax.invert_xaxis()
time_ax.set_xticks(new_tick_locations)
time_ax.set_xlim(ax.get_xlim())
plt.sca(ax)
time_ax.set_xticklabels([])
time_ax.set_xticklabels(['%.1f' % x for x in times])
time_ax.set_xlabel(r'Time [Gyr]', fontsize=fs)
time_ax.tick_params(axis='x', labelsize=fs-2)
time_ax.tick_params(axis='x', direction='out')
time_ax.tick_params(top=False, which='minor')
time_ax.minorticks_off()
for spine in ['left', 'right', 'bottom']:
  time_ax.spines[spine].set_visible(False)
#--------------------------------------------------------------------

# Size bar:
#--------------------------------------------------------------------


dots = 5 # Number of dots

dot_sizes = np.linspace(min_dot_size, max_dot_size, dots)
dot_masses = np.logspace(np.log10(min_mass), np.log10(max_mass), dots)

for dot_size, dot_mass in zip(dot_sizes, dot_masses):
  ax.scatter([],[], s=dot_size**2, c='black', lw=0, label=r'$%s\,$M$_{\odot}$' % latex_float(10**dot_mass))

# Choose the side to place the size bar:
if direction == 'left to right':
  location = 'upper left'
elif direction == 'right to left':
  location = 'upper right'
ax.legend(frameon=False, loc=location, fontsize=fs)





#--------------------------------------------------------------------

# Colorbar:
#--------------------------------------------------------------------
cbar_label = r'spin parameter $\lambda = L\sqrt{E}/GM^{2.5}$'


norm = mpl.colors.Normalize(vmin=cprop_min, vmax=cprop_max)
sm = cm.ScalarMappable(norm=norm, cmap=cmap)
sm.set_array([])
l, b, w, h = ax.get_position().bounds

# Choose the side to place the colourbar:
if direction == 'left to right':
  cax = fig.add_axes([l + w*(1+1/len(snapshots)), b, w*1/len(snapshots), h])
  cbar = plt.colorbar(sm, cax=cax)
  cax.yaxis.set_label_position('right')
  cax.yaxis.set_ticks_position('right')
elif direction == 'right to left':
  cax = fig.add_axes([l - w*2*(1/len(snapshots)), b, w*1/len(snapshots), h])
  cbar = plt.colorbar(sm, cax=cax)
  cax.yaxis.set_label_position('left')
  cax.yaxis.set_ticks_position('left')

cbar.set_label(cbar_label, fontsize=fs)
cbar.ax.tick_params(labelsize=fs-2)


#--------------------------------------------------------------------
plt.show()
plt.savefig(sim + '_merger_tree.pdf', bbox_inches='tight')
print('Done.')
