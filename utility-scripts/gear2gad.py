from pNbody import *
import numpy as np

for i in range(<min snap number>,<max snap number>):
	
	path = "<path to GEAR simulation snapshots>"
	
	nb = Nbody(path + "snapshot_" + "{:04d}".format(i))
	nb.tpe = np.where(nb.tpe==2,1,nb.tpe)
	nb.init()
	nb.flag_chimie_extraheader = 0
	nb.flag_metals = 0
	nb.flag_age = 0
	nb.rename("snapshot_" + "{:03d}".format(i))
	nb.write()
