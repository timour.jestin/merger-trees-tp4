



Merger Tree visualization routine by Timour Jestin - <timour.jestin@epfl.ch>

## Contents
* [Installation](#installation)
    1. [Rockstar & consistent trees](#rockstar-&-consistent-trees)
    2. [Tangos](#Tangos)
* [Running](#running)
    1. [Converting to GADGET](#converting-to-GADGET)
    2. [Halo cataloging](#halo-cataloging)
    3. [Adding to `Tangos` database](#adding-to-Tangos-database)
    4. [Visualisation](#Visualisation)
* [Additional documentation](#Additional-documentation)


## Installation <a name="installation"></a> ##

1. ### Rockstar & consistent trees <a name="rockstar-&-consistent-trees"></a> ###
    1. Move both folders to your working directory, such as your `/hpcstorage/` folder if working on `lesta`.
    2. Add the necessary options to the Rockstar `Makefile` (such as `DCHIMIE_EXTRAHEADER` when working with `GEAR`) 
    
    and modify the particle mass in `io_gadget`

    3. Compile both programs using `make` in the command prompt (use the   GNU C compiler version 4.0 or above on a 64-bit machine).

        The `Rockstar` and consistent trees codes do not support compiling on 32-bit machines, have not been tested with other compilers, and do not support non-Unix environments. 

        Furthermore, OpenMP is required to compile them.

2. ### Tangos <a name="Tangos"></a> ###
    1. Copy the `tangos` folder to your `/.local/lib/python3.9/site-packages/` folder.
    2. Install `pynbody` either through `pip install pynbody` or `pip install git+https://github.com/pynbody/pynbody.git`
    3. Set the following environment variables:
        ```
        export TANGOS_SIMULATION_FOLDER=/path/to/simulations/folder/
        export TANGOS_DB_CONNECTION=/path/to/sqlite.db
        ```
        with `TANGOS_SIMULATION_FOLDER` being the folder containing your simulations (e.g. in my case `/hpcstorage/tjestin/`) and `TANGOS_DB_CONNECTION` the path to were you want `Tangos` to create your database.
    
    4. If your `GADGET`/`GEAR` snapshot handles are not `snapshot_???`, adapt the `patterns` of the `GadgetSubfindInputHandler` class in `Tangos/input_handlers/pynbody.py`.
    
    Note that `Tangos` requires access to multiple python librairies including `matplotlib`, `numpy`, and `scipy`.

## Running <a name="running"></a> ##

1. ### Converting to GADGET <a name="converting-to-GADGET"></a> ###
   If you are still using `Tangos` in the version provided by this TP4 project, you first need to convert the `GEAR` files to `GADGET` files. This is thankfully very easy and fast using the `gear2gad.py` and `rename.slurm` scripts (in `/utility-scripts/`).
   You simply have to edit `gear2gad.py` by entering the path to your snapshots were needed, the minimum snapshot number and the maximum snapshot number. You can then launch the job on `lesta` using `rename.slurm`, which should take about 3 minutes for 250 snapshots on `p4` and `p5`.


2. ### Halo cataloging <a name="halo-cataloging"></a> ###
   To run `Rockstar` and consistent trees, modif your config file (such as `quickstart.cfg`) and `makeTree.slurm` accordingly (see the instructions in the files) and add them to your working directory (must be the one with the `GADGET`-like snapshots).
   You then run the halo cataloging and tree generation using `makeTree.slurm` (should take maximum 10 minutes for 250 snapshots on `p5`).

   
3. ### Adding to Tangos database <a name="adding-to-Tangos-database"></a> ###
    1. Make sure that `/home/astro/<your_id>/.local/lib/python3.9/site-packages/` is added to your `PYTHONPATH`
    2. Copy and modify `add_<sim_name>_tangos.slurm` with:
       1. `<sim_name>` the name of the folder in which your `GADGET`-like snapshots, `Rockstar` and consistent trees outputs are stored.\
       NOTA BENE : this will be the name under which your simulation will be accessible in python (using `tangos.get_simulation("<sim_name>")`). 
       2. `<properties you want to add>`. These are properties extracted by `Rockstar`, see the full `Rockstar` README to add new ones.
       3. You can adapt the minimum number of particles in your halos if your simulation is higher resolution. In that case 50 will probably induce a lot of "noise" halos that might not be interesting until they get bigger and will slow down the database storing process.
    3. run your new  `add_<sim_name>_tangos.slurm`. It is quite slow and takes about 4 hours on `p5` without paralelism.



4. ### Visualisation <a name="Visualisation"></a> ###
    To visualize your merger tree, modify and run `merger_tree_vis.py` with:
    1. Your `<sim_name>`
    2. An eventual `redshift_threshold` (0 by default).
    3. The property you want to log on the colourbar in `cprop` (must have been added in the `Tangos` database).
    4. Graphical options such as `scale_dots` and `line_colour` for readability and looks

## Additional documentation <a name="Additional-documentation"></a> ##
The READMEs for `Rockstar` and consistent trees are available in their respective folders, furthermore their respective repositories are :
<https://bitbucket.org/gfcstanford/rockstar/> and  <https://bitbucket.org/pbehroozi/consistent-trees/>. These also include links to the publications presenting these tools.

Aditional `Tangos` documentation is available at <https://pynbody.github.io/tangos/> and specific tools data exploration with Python are presented here: <https://nbviewer.org/github/pynbody/tangos/blob/master/docs/Data%20exploration%20with%20python.ipynb>

I remain available to help solve small issues and will update this `README` with common issues if needed.
